Source: pyglet
Section: python
Testsuite: autopkgtest-pkg-pybuild
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Michael Hanke <michael.hanke@gmail.com>,
           Per B. Sederberg <psederberg@gmail.com>,
           Stephan Peijnik <debian@sp.or.at>,
           Yaroslav Halchenko <debian@onerussian.com>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 flit,
 libavcodec-dev,
 libavformat-dev,
 libavutil-dev,
 libfontconfig-dev,
 libfreetype-dev,
 libgl-dev,
 libgl1-mesa-dri,
 libglu1-mesa,
 libpulse-dev,
 libswresample-dev,
 libswscale-dev,
 libx11-dev,
 libxext-dev,
 libxi-dev,
 libxinerama-dev,
 libxxf86vm-dev,
 pybuild-plugin-pyproject,
 python3-all,
 python3-png,
 python3-pytest,
 python3-setuptools,
 xauth,
 xvfb,
Standards-Version: 4.7.0
Homepage: http://www.pyglet.org
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/python-team/packages/pyglet.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pyglet

Package: python3-pyglet
Architecture: all
Depends:
 libfontconfig1,
 libfreetype6,
 libgl1 | libgl1-mesa-swx11,
 libglu1 | libglu1-mesa,
 libx11-6,
 libxext6,
 libxi6,
 libxinerama1,
 libxxf86vm1,
 python3-pillow,
 python3-png,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Recommends:
 libopenal1,
 libpulse0,
Suggests:
 libavcodec60 | libavcodec59 | libavcodec58,
 libavformat60 | libavformat59 | libavformat58,
 libavutil58 | libavutil57 | libavutil56,
 libswresample4 | libswresample3,
 libswscale7 | libswscale6 | libswscale5,
Description: cross-platform windowing and multimedia library (Python 3)
 This library provides an object-oriented programming interface for developing
 games and other visually-rich applications with Python.
 pyglet has virtually no external dependencies. For most applications and game
 requirements, pyglet needs nothing else besides Python, simplifying
 distribution and installation. It also handles multiple windows and
 fully aware of multi-monitor setups.
 .
 pyglet might be seen as an alternative to PyGame.
 .
 This is the Python 3 version.
